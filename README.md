# OpenML dataset: waveform-5000

https://www.openml.org/d/60

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Breiman,L., Friedman,J.H., Olshen,R.A., & Stone,C.J.  
**Source**: [UCI](http://archive.ics.uci.edu/ml/datasets/waveform+database+generator+(version+2)) - 1988  
**Please cite**: [UCI](http://archive.ics.uci.edu/ml/citation_policy.html)    

**Waveform Database Generator**  
Generator generating 3 classes of waves. Each class is generated from a combination of 2 of 3 "base" waves.  

For details, see Breiman,L., Friedman,J.H., Olshen,R.A., and Stone,C.J. (1984). 
Classification and Regression Trees. Wadsworth International, pp 49-55, 169. 

Note: There is [an earlier version](http://archive.ics.uci.edu/ml/datasets/Waveform+Database+Generator+(Version+1)) of this dataset that only has 21 attributes (it does not add the 19 noise features).

### Attribute Information

40 attributes describing the waveform, all of which include noise. The latter 19 attributes are all noise attributes with mean 0 and variance 1.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/60) of an [OpenML dataset](https://www.openml.org/d/60). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/60/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/60/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/60/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

